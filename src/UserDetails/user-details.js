
import React, {  useState, useEffect,useRef } from 'react';
import {  Container, Table, Button } from 'react-bootstrap';
import {  useNavigate } from 'react-router-dom';
import './user-details.css'
import axios from 'axios';
import moment from 'moment'

import { ToastContainer, toast } from 'react-toastify';
import { Audio } from 'react-loader-spinner'

function UserDetails() {

  const navigate = useNavigate();
  const [userList, setUserList] = useState([]);
  const [loading, setLoading] = useState(false);
  const dataFetchedRef = useRef(false);



  useEffect(() => {

    if (dataFetchedRef.current) return;
    dataFetchedRef.current = true;
    setLoading(true)
    axios.get('https://61763c6403178d00173dab00.mockapi.io/user').then(res => {
      setUserList(res.data);
      setLoading(false)

    })
      .catch(error => {
        return error;
      });

  }, []);



  const converDate = (date) => {
    var today = new Date(date);
    return moment(today).format('DD MMM YYYY');
  }

  const edit_user = (id, e) => {
    navigate(`/add-user?uid=${id}`)
  }


  const delete_user = (id, e) => {
    axios.delete(` https://61763c6403178d00173dab00.mockapi.io/user/${id}`).then(res => {
      var element = document.getElementById(id);
      element.remove();
      toast.success("Delete User Successfully !!!");
    })
      .catch(error => {
        return error;
      });

  }

  const add_user = () => {
    navigate(`/add-user`)
  }


  return (
    <>
      {!loading &&
        <Container>
          <Button onClick={(e) => add_user()} className='float-end create-btn' variant="primary">Create User</Button>

          <Table striped bordered hover>
            <thead className='table-color'>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Department</th>
                <th>Date of birth</th>
                <th>Email Id</th>
                <th>Phone Number</th>
                <th>Role</th>
                <th>Result</th>
                <th>Action</th>

              </tr>
            </thead>
            <tbody>
              {userList.map((x, i) =>
                <tr id={x.id} key={x.id}>
                  <td>{i + 1}</td>
                  <td>{x.name}</td>
                  <td>{x.department}</td>
                  <td>{converDate(x.date_of_birth)}</td>
                  <td>{x.email_id}</td>
                  <td>{x.phn_no}</td>
                  <td>{x.role}</td>
                  <td>{x.result}</td>
                  <td> <Button onClick={(e) => edit_user(x.id, e)} variant="warning">Edit</Button>   <Button onClick={(e) => delete_user(x.id, e)} variant="danger">Delete</Button></td>
                </tr>)}

              {userList.length === 0 && <tr>
                <td className="text-center" colSpan="12">
                  <b>No data found to display.</b>
                </td>
              </tr>}


            </tbody>
          </Table>
        </Container>
      }
      <ToastContainer />
      {loading &&
        <Audio
          height="100"
          width="100"
          // radius="48"
          color="#075599"
          // strokeWidth="4"
          ariaLabel="audio-loading"
          wrapperStyle={{ "align-items": "center", "height": "500px", "margin-left": "46%" }}
          wrapperClassName="spinnercls"
          visible={true}
        />}


    </>
  );

}



export default UserDetails;