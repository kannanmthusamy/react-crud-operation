
import './App.css';
import {Row,Container} from 'react-bootstrap';
import UserDetails from './UserDetails/user-details'
import AddUser from './add-edit-user/add-user'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-datepicker/dist/react-datepicker.css';

import 'react-toastify/dist/ReactToastify.css';

//import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';

import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

function App() {
  return (
<>
<Container>

<Row>
    <h2 className='Header'>User Management </h2>
</Row>


</Container>

<Router>
   

<Routes>
       <Route exact path="/" element={<UserDetails/>} />
       <Route  path="/add-user" element={<AddUser/>} /> 
       <Route  path="/add-user?uid=:id" element={<AddUser/>} /> 

         </Routes>
    
       </Router>
       </>
  );
}

export default App;
