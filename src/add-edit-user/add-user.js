
import React, { useState, useEffect ,useRef} from 'react';
import { Col, Row, Container, Button, Form, InputGroup } from 'react-bootstrap';
import { useNavigate,useSearchParams } from 'react-router-dom';


import { toast } from 'react-toastify';
import { Audio } from 'react-loader-spinner'


import axios from 'axios';
import moment from 'moment'


function AddUser() {

  const navigate = useNavigate();
  let [searchParams] = useSearchParams();

  const initialFormState = {
    id: null,
    name: "",
    department: "",
    date_of_birth: new Date().toISOString(),
    email_id: "",
    phn_no: "",
    role: "",
    result: "",
    isActive: true
  };

  const [btnName, setBtnName] = useState({ name: "Create" });
  const [validated, setValidated] = useState(false);
  const [user, setUser] = useState(initialFormState);
  const [loading, setLoading] = useState(false);
  const dataFetchedRef = useRef(false);

  const [query]=useState(searchParams.get('uid'))

  useEffect(() => {
    if (dataFetchedRef.current) return;
    dataFetchedRef.current = true;
    fetchingData ();
  }, [query]);


  const fetchingData = () => {

   
    if(query && query !=null) {

      setLoading(true)
      axios.get(`https://61763c6403178d00173dab00.mockapi.io/user/${query}`).then(res => {
        setUser(res.data);
        setBtnName({ name: 'update' })
        setLoading(false)
  
      })
        .catch(error => {
          setLoading(false)
          return error;
        });

    }

  };






  const handleInputChange = event => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };


  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      ApiCall();
      event.preventDefault();
    }

    setValidated(true);
  };


  const ApiCall = () => {


    if (query && query != null) {
      setLoading(true)
      axios.put(`https://61763c6403178d00173dab00.mockapi.io/user/${query}`, user).then(res => {
        toast.success("Update User Successfully !!!");
        setLoading(false)
        navigate('/', { replace: true });
      })
        .catch(error => {
          setLoading(false)
          return error;
        });

    } else {

      setLoading(true)
      axios.post('https://61763c6403178d00173dab00.mockapi.io/user', user).then(res => {
        toast.success("Add User Successfully !!!");
        setLoading(false)
        navigate('/', { replace: true });
      })
        .catch(error => {
          setLoading(false)
          return error;
        });
    }

  }

  const converDate = (date) => {
    return moment(date).format('YYYY-MM-DD');
  }

  return (
    <>

      {!loading &&
        <Container className=' mt-5 mb-5'>


          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Row className="mb-3">
              <Form.Group as={Col} md="4" controlId="validationCustom01">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  required
                  type="text"
                  name="name"
                  value={user.name}
                  onChange={handleInputChange}
                  placeholder="Enter name"
                />
                <Form.Control.Feedback type="invalid">
                  Please choose a Name.
                </Form.Control.Feedback>

              </Form.Group>
              <Form.Group as={Col} md="4" controlId="validationCustom02">
                <Form.Label>Department</Form.Label>
                <Form.Control
                  required
                  type="text"
                  name="department"
                  value={user.department}
                  onChange={handleInputChange}
                  placeholder="Enter Department"
                />
                <Form.Control.Feedback type="invalid">
                  Please choose a Department.
                </Form.Control.Feedback>
              </Form.Group>


              <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                <Form.Label>Email Id</Form.Label>
                <InputGroup hasValidation>
                  <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                  <Form.Control
                    type="email"
                    placeholder="Enter Email Id"
                    name="email_id"
                    value={user.email_id}
                    onChange={handleInputChange}
                    aria-describedby="inputGroupPrepend"
                    required
                  />
                  <Form.Control.Feedback type="invalid">
                    Please choose a Valid Email ID
                  </Form.Control.Feedback>

                </InputGroup>
              </Form.Group>
            </Row>


            <Row className='mt-4'>

              <Form.Group as={Col} md="6" controlId="validationCustom03">
                <Form.Label>Date of Birth </Form.Label>
                <Form.Control type="date"
                  name="date_of_birth"
                  value={converDate(user.date_of_birth)}
                  onChange={handleInputChange}
                  placeholder="Enter Date Of Birth"
                  required />

                {/* <DatePicker
            id="test"
            required
            name="date_of_birth"
            value = {user.date_of_birth}
            selected={startDate}
            onChange={handleInputChange}
            // onChangeRaw={event => handleChangeRaw(event.target.value)}
            placeholderText="mm/dd/yyyy"
            // className={valid ? "form-control " : "form-control is-invalid"}
          /> */}
                <Form.Control.Feedback type="invalid">
                  Please choose a Date of Birth.
                </Form.Control.Feedback>
              </Form.Group>


              <Form.Group as={Col} md="6" controlId="validationCustom04">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control type="number"
                  name="phn_no"
                  value={user.phn_no}
                  onChange={handleInputChange}
                  placeholder="Enter Phone Number"
                  required />
                <Form.Control.Feedback type="invalid">
                  Please provide a Valid Phone Number
                </Form.Control.Feedback>
              </Form.Group>

            </Row>

            <Row className="mt-4 mb-4">


              <Form.Group as={Col} md="6" controlId="validationCustom05">
                <Form.Label>Role</Form.Label>
                <Form.Control type="text"
                  name="role"
                  onChange={handleInputChange}
                  placeholder="Enter role"
                  value={user.role}
                  required />
                <Form.Control.Feedback type="invalid">
                  Please provide a Role.
                </Form.Control.Feedback>
              </Form.Group>

              <Form.Group as={Col} md="6" controlId="validationCustom05">
                <Form.Label>Result</Form.Label>
                <Form.Control type="text"
                  name="result"
                  value={user.result}
                  onChange={handleInputChange}
                  placeholder="Enter Result"
                  required />
                <Form.Control.Feedback type="invalid">
                  Please provide a Result.
                </Form.Control.Feedback>
              </Form.Group>


            </Row>

            <Button typeof='submit' variant="primary" type="submit">
              {btnName.name}
            </Button>

            <Button type="reset" className='float-end' variant="danger" >
              Reset
            </Button>

          </Form>

        </Container>
      }
      {loading &&
        <Audio
          height="100"
          width="100"
          // radius="48"
          color="#075599"
          // strokeWidth="4"
          ariaLabel="audio-loading"
          wrapperStyle={{ "align-items": "center", "height": "500px", "margin-left": "46%" }}
          wrapperClassName="spinnercls"
          visible={true}
        />}

    </>
  );

}



export default AddUser;