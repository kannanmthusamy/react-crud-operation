import axios from "axios";

export default axios.create({
    api: 'https://61763c6403178d00173dab00.mockapi.io/',
  headers: {
    "Content-type": "application/json"
  }
});